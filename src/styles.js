import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxHeight: "100vh",
  },
  card: {
    maxWidth: "100%",
  },
  paragraph: {
    marginBlockStart: 0,
    marginBlockEnd: 0,
  },
  title: {
    flexGrow: 1,
    fontSize: 14,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  labelOutput: {
    fontSize: 18,
  },
  textFieldForm: {
    width: "100%",
  },
}));

export default useStyles;
