import React, { useState } from "react";
import {
  Container,
  Grid,
  TextField,
  Typography,
  Button,
  Card,
  CardContent,
} from "@material-ui/core";

import useStyles from "./styles";
import Axios from "axios";

const FR_NET = () => {
  const classes = useStyles();
  const [result, setResult] = useState();
  const [image, setImage] = useState();
  const [page, setPage] = useState(false);
  const [accuracy, setAcc] = useState();
  const [output, setOutput] = useState();
  const scanAction = async () => {
    setResult(null);
    setImage(null);
    setAcc(null);
    const input = document.getElementById("files");
    if (input.files.length > 0) {
      try {
        setResult("upload photo...");
        const formData = new FormData();
        for (let i = 0; i !== input.files.length; i++) {
          formData.append("files", input.files[i]);
        }
        const response = await Axios({
          method: "POST",
          url: "http://mk-cideng.ddns.net:5000/ConsoleFR/Upload",
          data: formData,
        });
        if (response.status === 200 && response.data.getname) {
          const { getname } = response.data;
          setResult("scanning photo...");
          const scan_fr = await Axios({
            method: "POST",
            url:
              "http://mk-cideng.ddns.net:5000/api/FRSingleImageUpload/GetFacialRecognition",
            data: { direktori: getname },
          });
          if (scan_fr.status === 200) {
            const {
              pesan,
              gambarcombine,
              pesancode,
              hasil,
              responseMessage,
            } = scan_fr.data;
            setOutput(scan_fr.data);
            setResult(`[${responseMessage}] ${pesan}`);
            setImage(gambarcombine);
            setAcc(hasil);
            // if (pesancode === 5) {
            //   setTimeout(() => {
            //     setPage(!page);
            //   }, 3000);
            // }
          }
        }
      } catch (error) {
        setResult(error.message);
      }
    } else {
      setResult("please input required file");
    }
  };
  return (
    <Container fixed className={classes.root}>
      {page ? (
        <Grid
          container
          spacing={3}
          direction="column"
          style={{ padding: "20px 0" }}
          justify="center"
          alignItems="center"
        >
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Typography variant="h5">
              {"face recognition success".toUpperCase()}
            </Typography>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              onClick={() => {
                setPage(false);
                setResult(null);
                setImage(null);
                setAcc(null);
              }}
            >
              Back to verification
            </Button>
          </Grid>
        </Grid>
      ) : (
        <Grid container spacing={2} justify="flex-start" alignItems="center">
          <Grid item lg={12} md={12} sm={12} xs={12}>
            <Typography variant="h5">Face Recognition Page</Typography>
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <TextField
              inputProps={{ id: "files" }}
              type="file"
              label="Input File"
              helperText="Please input required file"
              onChange={(e) => {
                const output = document.getElementById("output");
                if (e.target.files.length > 0) {
                  output.src = URL.createObjectURL(e.target.files[0]);
                } else {
                  output.src = "";
                }
                output.onload = function () {
                  URL.revokeObjectURL(output.src); // free memory
                };
              }}
            />
          </Grid>
          <Grid item lg={6} md={6} sm={6} xs={6}>
            <Button variant="contained" color="primary" onClick={scanAction}>
              Scan
            </Button>
          </Grid>
          <Grid item sm={12} xs={12} lg={6} md={6}>
            <img id="output" alt="" style={{ width: "100%" }} />
          </Grid>
          <Grid item lg={7} md={7} sm={12} xs={12}>
            <Card variant="outlined">
              <CardContent>
                {result && <Typography variant="p">{result}</Typography>}
                <br />
                {output?.pesancode === 5 && (
                  <Typography variant="p">
                    Hasil: {output.kesimpulan}
                  </Typography>
                )}

                {image && (
                  <img
                    style={{ width: "100%", margin: "10px 0 0 0" }}
                    src={`data:image/jpeg;base64,${image}`}
                    alt="Image Result"
                  />
                )}
              </CardContent>
            </Card>
          </Grid>
        </Grid>
      )}
    </Container>
  );
};

export default FR_NET;
