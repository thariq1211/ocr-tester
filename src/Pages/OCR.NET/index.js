import React, { useState } from "react";
import Axios from "axios";
import {
  Container,
  Typography,
  FormControl,
  Card,
  CardContent,
  FormHelperText,
  Input,
  InputLabel,
  Grid,
  Slider,
  Button,
  TextField,
} from "@material-ui/core";
import { Brightness4, Brightness5 } from "@material-ui/icons";

import useStyles from "../../styles";
import Constanta from "../../constanta";

const OCR_NET = () => {
  const classes = useStyles();
  const url = Constanta.url_net;
  const [result, setResult] = useState("Result will show here");
  const [data, setData] = useState("");
  const [rawData, setRawData] = useState([]);
  const [contrast, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const prosesOcr = async () => {
    setData("");
    const input = document.getElementById("fileInput").files;
    if (input.length === 1 && contrast) {
      try {
        setResult("Uploading file..");
        const formData = new FormData();
        for (var i = 0; i !== input.length; i++) {
          formData.append("files", input[i]);
        }
        const response = await Axios({
          url: `${url}/ConsoleOCR/Upload`,
          method: "POST",
          data: formData,
        });
        if (response.status === 200) {
          setResult("Upload success..");
          const { getname } = response.data;
          try {
            setResult("Scanning KTP..");
            /**
             * http://mk-cideng.ddns.net:5003/api/GetOCR/GetNewOCRScanServices
             */
            const responseOcr = await Axios({
              url: `${url}/api/GetOCR/GetNewOCRScanServices`,
              method: "POST",
              data: { direktori: getname, contrast },
            });
            if (responseOcr.status === 200) {
              const { data } = responseOcr;
              setResult("");
              setData({
                provinsi: data[0],
                kota: data[1],
                nik: data[2],
                nama: data[3],
                jk: data[4],
                ttl: data[5],
                alamat: `${data[6]} ${data[7]} ${data[8]} ${data[9]} ${data[10]}`,
                kawin: data[12],
                pekerjaan: data[13],
                agama: data[11],
                berlaku: data[15],
              });
              setRawData(data);
            }
          } catch (error) {
            setResult(error.message);
          }
        }
      } catch (error) {
        setResult(error.message);
      }
    } else if (contrast === 0 && input.length === 1) {
      setResult("Please set contrast properly");
    } else {
      setResult("Please input required file");
    }
  };

  const dataOutput = (data) => {
    return (
      <>
        <p className={classes.paragraph}>{data.provinsi}</p>
        <p className={classes.paragraph}>{data.kota}</p>
        <p className={classes.paragraph}>{data.nik}</p>
        <p className={classes.paragraph}>{data.nama}</p>
        <p className={classes.paragraph}>{data.jk}</p>
        <p className={classes.paragraph}>{data.ttl}</p>
        <p className={classes.paragraph}>{data.alamat}</p>
        <p className={classes.paragraph}>{data.kawin}</p>
        <p className={classes.paragraph}>{data.pekerjaan}</p>
        <p className={classes.paragraph}>{data.agama}</p>
        <p className={classes.paragraph}>{data.berlaku}</p>
      </>
    );
  };

  return (
    <>
      <Container fixed>
        <br />
        <Typography variant="h5">OCR .NET</Typography>
        <br />
        <FormControl style={{ width: "40vw" }}>
          <Typography id="continuous-slider" gutterBottom>
            Contrast
          </Typography>
          <Grid container spacing={2}>
            <Grid item>
              <Brightness5 />
            </Grid>
            <Grid item xs md lg sm>
              <Slider
                value={contrast}
                onChange={handleChange}
                aria-labelledby="continuous-slider"
                valueLabelDisplay="auto"
              />
            </Grid>
            <Grid item>
              <Brightness4 />
            </Grid>
          </Grid>
        </FormControl>
        <br />
        <Grid container spacing={2} justify="flex-start" alignItems="center">
          <Grid item sm={12} xs={12} md={6} lg={6}>
            <FormControl>
              <InputLabel htmlFor="fileInput">File OCR</InputLabel>
              <Input
                id="fileInput"
                type="file"
                aria-describedby="my-helper-text"
                onChange={(e) => {
                  const output = document.getElementById("output");
                  if (e.target.files.length > 0) {
                    output.src = URL.createObjectURL(e.target.files[0]);
                  } else {
                    output.src = "";
                  }
                  output.onload = function () {
                    URL.revokeObjectURL(output.src); // free memory
                  };
                }}
              />
              <FormHelperText id="my-helper-text">
                Please input required file
              </FormHelperText>
            </FormControl>
          </Grid>
          <Grid item md={6} lg={6}>
            <Button color="primary" variant="contained" onClick={prosesOcr}>
              Scan
            </Button>
          </Grid>
          <Grid item sm={12} xs={12} lg={8} md={8}>
            <img id="output" alt="" style={{ width: "80%" }} />
          </Grid>
        </Grid>

        <br />
        <br />
        <Card className={classes.card} variant="outlined">
          <CardContent>
            <Typography
              className={classes.title}
              color="textSecondary"
              gutterBottom
            >
              Result
            </Typography>
            <Typography variant="subtitle1">{result}</Typography>
            <Typography variant="subtitle1">
              {data && dataOutput(data)}
            </Typography>
          </CardContent>
        </Card>
        <br />
        <Card className={classes.card} variant="outlined">
          <CardContent>
            {rawData.length > 0 &&
              rawData.map((item) => (
                <>
                  <TextField
                    disabled
                    readOnly
                    size="small"
                    defaultValue={item}
                    variant="outlined"
                    style={{ width: "100%", margin: "5px" }}
                  />
                  <br />
                </>
              ))}
          </CardContent>
        </Card>
      </Container>
    </>
  );
};
export default OCR_NET;
