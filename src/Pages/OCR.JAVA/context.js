import React, { useReducer } from "react";
import { initialState, reducer } from "./reducers";

export const OcrContext = React.createContext();

const OcrContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const setMatrix = (payload) => {
    dispatch({ type: "SET_MATRIX", payload });
  };
  const updateMatrix = ({ value, key, e }) => {
    dispatch({ type: "UPDATE_MATRIX", payload: { value, key, e } });
  };
  const clearMatrix = () => {
    dispatch({ type: "CLEAR_MATRIX" });
  };
  const exported = {
    matrixCont: state.matrix,
    setMatrixCont: setMatrix,
    updateMatrixCont: updateMatrix,
    clearMatrix,
  };
  return <OcrContext.Provider value={exported}>{children}</OcrContext.Provider>;
};

export default OcrContextProvider;
