export const initialState = {
  matrix: [],
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case "SET_MATRIX":
      return { ...state, matrix: action.payload };
    case "CLEAR_MATRIX":
      return { ...state, matrix: [] };
    case "UPDATE_MATRIX":
      const currentState = state.matrix;
      const payload = action.payload;
      const newState = currentState.map((item) => {
        return item.fieldId === payload.e.fieldId
          ? { ...item, [payload.key]: payload.value }
          : item;
      });
      return { ...state, matrix: newState };
    default:
      return state;
  }
};
