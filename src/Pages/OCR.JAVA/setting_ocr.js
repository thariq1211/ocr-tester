import React, { useState, useEffect, memo, useContext } from "react";
import Axios from "axios";
import {
  Card,
  CardHeader,
  CardContent,
  IconButton,
  InputAdornment,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TextField,
  FormControlLabel,
  Switch,
  Paper,
  Typography,
  Avatar,
} from "@material-ui/core";
import { Close, Edit, Save, Settings } from "@material-ui/icons";

import { OcrContext } from "./context";

import Constanta from "../../constanta";

const Setting = memo(
  ({ setOpenBackdrop, backDropOpen, documentType, setType }) => {
    const { updateMatrixCont, matrixCont, setMatrixCont } = useContext(
      OcrContext
    );
    const url = Constanta.url_java;
    const [matrix, setMatrix] = useState([]);
    const [edit, setEdit] = useState([]);
    const [save, setSave] = useState([]);

    const columns = [
      { id: "fieldId", label: "", minWidth: "100", align: "center" },
      { id: "fieldName", label: "Field Name", minWidth: "170", align: "left" },
      {
        id: "regex",
        label: "Regex",
        minWidth: "150",
        align: "left",
        format: (val) => val.toLocaleString("en-US"),
      },
      {
        id: "dictionary",
        label: "Dictionary",
        minWidth: "170",
        align: "left",
        format: (val) => val.toLocaleString("en-US"),
      },
      {
        id: "keyword",
        label: "Keywords",
        minWidth: "170",
        align: "left",
        format: (val) => val.toLocaleString("en-US"),
      },
      {
        id: "accuracy",
        label: "Accuracy",
        minWidth: "80",
        align: "center",
      },
      {
        id: "isNewLine",
        label: "New Line",
        align: "left",
        minWidth: "170",
      },
      {
        id: "isTitle",
        label: "Title",
        align: "left",
        minWidth: "170",
      },
      {
        id: "isMultipleLines",
        label: "Multiple Lines",
        align: "left",
        minWidth: "170",
      },
      {
        id: "action",
        label: "Action",
        align: "center",
        minWidth: "200",
      },
    ];

    const prosesMatrix = async () => {
      setMatrix([]);
      try {
        const request = await Axios({
          url: `${url}/ocr/setting/matrix`,
          method: "POST",
          data: { documentName: documentType.toUpperCase() },
        });
        if (request.status === 200) {
          setMatrix(request.data);
          setMatrixCont(request.data);
          setEdit(
            request.data.map((e, index) => ({
              id: e.fieldId,
              statusEdit: false,
            }))
          );
          setSave(
            request.data.map((e, index) => ({
              id: e.fieldId,
              statusSave: false,
            }))
          );
        }
      } catch (error) {
        console.log(error.message);
      }
    };

    function updateMatrix(setMatrix, e, key) {
      return (event) => {
        setMatrix((prevMatrix) =>
          prevMatrix.map((item) => {
            return item.fieldId === e.fieldId
              ? {
                  ...item,
                  [key]: event.target.checked,
                }
              : item;
          })
        );
      };
    }

    function getDefaultValue(matrix, e, key) {
      return matrix.filter((item) => item.fieldId === e.fieldId)[0]?.[key];
    }

    function changeField(setMatrix, e, key) {
      return (ev) => {
        updateMatrixCont({ value: ev.target.value, key, e });

        setMatrix((prevMatrix) =>
          prevMatrix.map((item) => {
            return item.fieldId === e.fieldId
              ? {
                  ...item,
                  [key]: ev.target.value,
                }
              : item;
          })
        );
      };
    }

    function getIsDisabled(save, e) {
      return !save.filter((item) => {
        return item.id === e.fieldId;
      })[0]?.statusSave;
    }

    function getIsChecked(matrix, e, key) {
      return matrix.filter((item) => item.fieldId === e.fieldId)[0]?.[key];
    }

    function buttonClick(setEdit, e, setSave) {
      return () => {
        setEdit((prevEdit) =>
          prevEdit.map((item) => {
            return item.id === e.fieldId
              ? {
                  ...item,
                  statusEdit: !item.statusEdit,
                }
              : item;
          })
        );
        setSave((prevSave) =>
          prevSave.map((item) => {
            return item.id === e.fieldId
              ? {
                  ...item,
                  statusSave: !item.statusSave,
                }
              : item;
          })
        );
      };
    }

    useEffect(() => {
      // if (
      //   documentType !== undefined &&
      //   documentType !== null &&
      //   documentType !== ""
      // )
      //   prosesMatrix();
      if (documentType === null) setMatrix([]);
    }, [documentType]);

    useEffect(() => {
      if (backDropOpen === false) {
        setMatrix([]);
      }
      if (backDropOpen && documentType !== "") prosesMatrix();
    }, [backDropOpen]);

    return (
      <Card style={{ width: "90%", height: "90%" }}>
        <CardHeader
          action={
            <IconButton onClick={() => setOpenBackdrop(false)}>
              <Close />
            </IconButton>
          }
          title="Settings matrix"
          avatar={
            <>
              <Avatar style={{ backgroundColor: "#3f51b5" }}>
                <Settings />
              </Avatar>
            </>
            // <FormControl style={{ width: "250px" }}>
            //   <InputLabel id="setting-document">Document type</InputLabel>
            //   <Select
            //     labelId="demo-simple-select-helper-label"
            //     id="setting-document"
            //     onChange={(e) => setType(e.target.value)}
            //     value={documentType}
            //   >
            //     <MenuItem value={undefined}>
            //       <em>None</em>
            //     </MenuItem>
            //     <MenuItem value={"ktp"}>KTP</MenuItem>
            //     <MenuItem value={"npwp"}>NPWP</MenuItem>
            //   </Select>
            //   <FormHelperText>Select document type</FormHelperText>
            // </FormControl>
          }
        ></CardHeader>
        <CardContent>
          <TableContainer component={Paper} style={{ maxHeight: "70vh" }}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  {columns.map((column) => (
                    <TableCell
                      key={column.id}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {column.label}
                    </TableCell>
                  ))}
                </TableRow>
              </TableHead>
              <TableBody>
                {matrix.length > 0 ? (
                  matrix.map((e) => (
                    <TableRow>
                      <TableCell hidden id={e.fieldId}></TableCell>
                      <TableCell align="left" style={{ width: 140 }}>
                        <TextField
                          inputProps={{
                            style: {
                              fontSize: "10.5pt",
                            },
                          }}
                          multiline
                          rowsMax="2"
                          disabled={getIsDisabled(save, e)}
                          variant="outlined"
                          size="small"
                          defaultValue={getDefaultValue(matrix, e, "fieldName")}
                          onChange={changeField(setMatrix, e, "fieldName")}
                        />
                      </TableCell>
                      <TableCell align="left" style={{ width: 160 }}>
                        <TextField
                          inputProps={{
                            style: {
                              fontSize: "10.5pt",
                            },
                          }}
                          multiline
                          rowsMax="2"
                          disabled={getIsDisabled(save, e)}
                          variant="outlined"
                          size="small"
                          defaultValue={getDefaultValue(matrix, e, "regex")}
                          onChange={changeField(setMatrix, e, "regex")}
                        />
                      </TableCell>
                      <TableCell align="left" style={{ width: 150 }}>
                        <TextField
                          inputProps={{
                            style: {
                              fontSize: "10.5pt",
                            },
                          }}
                          multiline
                          rowsMax="2"
                          disabled={getIsDisabled(save, e)}
                          variant="outlined"
                          size="small"
                          defaultValue={getDefaultValue(
                            matrix,
                            e,
                            "dictionary"
                          )}
                          onChange={changeField(setMatrix, e, "dictionary")}
                        />
                      </TableCell>
                      <TableCell align="left" style={{ width: 140 }}>
                        <TextField
                          inputProps={{
                            style: {
                              fontSize: "10.5pt",
                            },
                          }}
                          multiline
                          rowsMax="2"
                          disabled={getIsDisabled(save, e)}
                          variant="outlined"
                          size="small"
                          defaultValue={getDefaultValue(matrix, e, "keywords")}
                          onChange={changeField(setMatrix, e, "keywords")}
                        />
                      </TableCell>
                      <TableCell align="center" style={{ width: 100 }}>
                        <TextField
                          inputProps={{
                            style: {
                              fontSize: "10.5pt",
                            },
                            step: 1,
                            min: 0,
                            max: 100,
                          }}
                          InputProps={{
                            endAdornment: (
                              <InputAdornment position="end">%</InputAdornment>
                            ),
                          }}
                          size="small"
                          variant="outlined"
                          type="number"
                          disabled={getIsDisabled(save, e)}
                          defaultValue={
                            getDefaultValue(matrix, e, "accuracy") * 100
                          }
                          onChange={changeField(setMatrix, e, "accuracy")}
                        />
                      </TableCell>
                      <TableCell align="center">
                        <FormControlLabel
                          control={
                            <Switch
                              disabled={getIsDisabled(save, e)}
                              size="small"
                              checked={getIsChecked(matrix, e, "isNewLine")}
                              onChange={updateMatrix(setMatrix, e, "isNewLine")}
                              name="isNewLine"
                              color="primary"
                            />
                          }
                        />
                      </TableCell>
                      <TableCell align="center">
                        <FormControlLabel
                          control={
                            <Switch
                              disabled={getIsDisabled(save, e)}
                              size="small"
                              checked={getIsChecked(matrix, e, "isTitle")}
                              onChange={updateMatrix(setMatrix, e, "isTitle")}
                              name="isTitle"
                              color="primary"
                            />
                          }
                        />
                      </TableCell>
                      <TableCell align="center">
                        <FormControlLabel
                          control={
                            <Switch
                              disabled={getIsDisabled(save, e)}
                              size="small"
                              checked={getIsChecked(
                                matrix,
                                e,
                                "isMultipleLines"
                              )}
                              onChange={updateMatrix(
                                setMatrix,
                                e,
                                "isMultipleLines"
                              )}
                              name="isMultipleLines"
                              color="primary"
                            />
                          }
                        />
                      </TableCell>
                      <TableCell align="center" style={{ width: 100 }}>
                        <IconButton
                          size="small"
                          disabled={
                            edit.filter((item) => {
                              return item.id === e.fieldId;
                            })[0]?.statusEdit
                          }
                          onClick={buttonClick(setEdit, e, setSave)}
                          color="secondary"
                        >
                          <Edit />
                        </IconButton>
                        <IconButton
                          size="small"
                          disabled={
                            !save.filter((item) => {
                              return item.id === e.fieldId;
                            })[0]?.statusSave
                          }
                          onClick={() => {
                            setEdit((prevEdit) =>
                              prevEdit.map((item) => {
                                return item.id === e.fieldId
                                  ? {
                                      ...item,
                                      statusEdit: !item.statusEdit,
                                    }
                                  : item;
                              })
                            );
                            setSave((prevSave) =>
                              prevSave.map((item) => {
                                return item.id === e.fieldId
                                  ? {
                                      ...item,
                                      statusSave: !item.statusSave,
                                    }
                                  : item;
                              })
                            );
                            const currentMatrix = matrix.filter(
                              (item) => item.fieldId === e.fieldId
                            )[0];
                            Axios({
                              url: `${url}/ocr/setting/field/save`,
                              method: "POST",
                              data: {
                                fieldId: currentMatrix.fieldId,
                                fieldName: currentMatrix.fieldName,
                                regex: currentMatrix.regex,
                                keywords: currentMatrix.keywords
                                  .split(",")
                                  .map((i) => i.trim())
                                  .join(","),
                                dictionary: currentMatrix.dictionary
                                  .split(",")
                                  .map((i) => i.trim())
                                  .join(","),
                                isNewLine: currentMatrix.isNewLine,
                                isTitle: currentMatrix.isTitle,
                                isMultipleLines: currentMatrix.isMultipleLines,
                                accuracy:
                                  !isNaN(currentMatrix.accuracy) &&
                                  currentMatrix.accuracy
                                    .toString()
                                    .indexOf(".") != -1
                                    ? currentMatrix.accuracy
                                    : parseInt(currentMatrix.accuracy) / 100,
                              },
                            })
                              .then((res) => alert(res.data.message))
                              .catch((err) => alert(err.message));
                          }}
                          color="primary"
                        >
                          <Save />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))
                ) : (
                  <Typography variant="p">Please wait</Typography>
                )}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
    );
  }
);

export default Setting;
