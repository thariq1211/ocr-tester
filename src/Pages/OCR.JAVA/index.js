import React, { useState, memo, useContext } from "react";
import Axios from "axios";
import {
  Container,
  FormControl,
  Card,
  CardContent,
  FormHelperText,
  Input,
  InputLabel,
  Grid,
  Button,
  Typography,
  TextField,
  Select,
  MenuItem,
  Fab,
  makeStyles,
  Tooltip,
  Backdrop,
} from "@material-ui/core";

import { Settings } from "@material-ui/icons";

import useStyles from "../../styles";
import Constanta from "../../constanta";

import { OcrContext } from "./context";

const Setting = React.lazy(() => import("./setting_ocr"));

const OutputLabel = ({ rowLabel, value = " " }) => {
  const classes = useStyles();
  return (
    <>
      <Grid item md={3} lg={3} xs={9} sm={9}>
        <Typography className={classes.labelOutput} color="textSecondary">
          {rowLabel}
        </Typography>
      </Grid>
      <Grid item md={1} lg={1} xs={3} sm={3}>
        <Typography className={classes.labelOutput} color="textSecondary">
          :
        </Typography>
      </Grid>
      <Grid item md={8} lg={8} xs={12} sm={12}>
        <FormControl className={classes.textFieldForm}>
          <TextField
            size="small"
            id="outlined-basic"
            label={value}
            variant="outlined"
            disabled
          />
        </FormControl>
      </Grid>
      {/* one row output */}
    </>
  );
};

const javaOcrStyles = makeStyles((theme) => ({
  root: {
    // position: "relative",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  fab: {
    position: "absolute",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

const OCR_JAVA = memo(() => {
  const classes = useStyles();
  const classOcrJava = javaOcrStyles();
  const url = Constanta.url_java;
  const { matrix } = useContext(OcrContext);
  // console.log(matrix);
  const [result, setResult] = useState("Result will show here");
  const [data, setData] = useState("");
  const [, setObjectResult] = useState();
  const [backdropOpen, setOpenBackdrop] = useState(false);
  const [documentType, setType] = useState("");
  const [arrayData, setArrData] = useState([]);

  const prosesOcr = async () => {
    setData("");
    setArrData([]);
    setObjectResult(undefined);
    const input = document.getElementById("fileInput").files;
    if (input.length === 1 && documentType !== "") {
      try {
        setResult("Uploading file..");
        const formData = new FormData();
        for (var i = 0; i !== input.length; i++) {
          formData.append("image", input[i]);
        }
        const response = await Axios({
          url: `${url}/ocr/document/${documentType}`,
          method: "POST",
          data: formData,
        });
        if (response.status === 200) {
          setResult("Upload success..");
          const { ocrResult, objectResult } = response.data;
          setResult(ocrResult);
          if (documentType === "ktp") {
            setObjectResult({
              provinsi: objectResult.provinsi,
              kota_kab: objectResult.kota_kab,
              nik: objectResult.nik,
              nama: objectResult.nama,
              tempattgllahir: objectResult.tempattgllahir,
              jeniskelamin: objectResult.jeniskelamin,
              goldarah: objectResult.goldarah,
              alamat: objectResult.alamat,
              rtrw: objectResult.rtrw,
              keldesa: objectResult.keldesa,
              kecamatan: objectResult.kecamatan,
              agama: objectResult.agama,
              statusperkawinan: objectResult.statusperkawinan,
              pekerjaan: objectResult.pekerjaan,
              kewarganegaraan: objectResult.kewarganegaraan,
              berlakuhingga: objectResult.berlakuhingga,
            });
            setArrData(objectResult);
          } else {
            setObjectResult({
              npwp: objectResult.npwp,
              nama: objectResult.nama,
              nik: objectResult.nik,
              alamat: objectResult.alamat,
              kpp: objectResult.kpp,
              terdaftar: objectResult.terdaftar,
            });
            setArrData(objectResult);
          }
        }
      } catch (error) {
        setResult(error.message);
      }
    }
    // else if (documentType === "npwp") {
    //   setResult("Still under development");
    // }
    else {
      setResult("Please input required file and document type");
    }
  };

  return (
    <>
      <Container fixed>
        <br />
        <Typography variant="h5">OCR JAVA</Typography>
        <br />
        <Grid container spacing={2} justify="flex-start" alignItems="center">
          <Grid item md={12} lg={12} sm={12} xs={12}>
            <FormControl style={{ width: "40vw" }}>
              <InputLabel id="demo-simple-select-helper-label">
                Document Type
              </InputLabel>
              <Select
                labelId="demo-simple-select-helper-label"
                id="demo-simple-select-helper"
                onChange={(e) => {
                  setType(e.target.value);
                  setArrData([]);
                  setData("");
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value={"ktp"}>KTP</MenuItem>
                <MenuItem value={"npwp"}>NPWP</MenuItem>
              </Select>
              <FormHelperText>Please select document type</FormHelperText>
            </FormControl>
          </Grid>
          <Grid item sm={6} xs={6}>
            <FormControl style={{ width: "40vw" }}>
              <InputLabel htmlFor="fileInput">File OCR</InputLabel>
              <Input
                id="fileInput"
                type="file"
                aria-describedby="my-helper-text"
                onChange={(e) => {
                  const output = document.getElementById("output");
                  if (e.target.files.length > 0) {
                    output.src = URL.createObjectURL(e.target.files[0]);
                  } else {
                    output.src = "";
                  }
                  output.onload = function () {
                    URL.revokeObjectURL(output.src); // free memory
                  };
                }}
              />
              <FormHelperText id="my-helper-text">
                Please input required file
              </FormHelperText>
            </FormControl>
          </Grid>
          <Grid item sm={6} xs={6}>
            <Button color="primary" variant="contained" onClick={prosesOcr}>
              Scan
            </Button>
          </Grid>
          <Grid item sm={12} xs={12} lg={6} md={6}>
            <img id="output" alt="" style={{ width: "100%" }} />
          </Grid>
        </Grid>
        <br />
        <br />
        <Grid container spacing={2}>
          <Grid item md={9} lg={9} sm={12} xs={12}>
            <Card className={classes.card} variant="outlined">
              <CardContent>
                <Typography
                  className={classes.title}
                  color="textSecondary"
                  gutterBottom
                >
                  Result
                </Typography>
                <Typography variant="subtitle1">{result}</Typography>
                <Typography variant="subtitle1">{data}</Typography>
              </CardContent>
            </Card>
          </Grid>
          {documentType === "ktp" ? (
            <Grid item md={9} lg={9} xs={12} sm={12}>
              <Card className={classes.card} variant="outlined">
                <CardContent>
                  <Grid
                    spacing={1}
                    container
                    justify="flex-start"
                    alignItems="center"
                  >
                    {arrayData.length > 0 &&
                      arrayData.map((e) => (
                        <OutputLabel rowLabel={e.fieldName} value={e.result} />
                      ))}
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
          ) : documentType === "npwp" ? (
            <Grid item md={9} lg={9} xs={12} sm={12}>
              <Card className={classes.card} variant="outlined">
                <CardContent>
                  <Grid
                    spacing={1}
                    container
                    justify="flex-start"
                    alignItems="center"
                  >
                    {arrayData.length > 0 &&
                      arrayData.map((e) => (
                        <OutputLabel rowLabel={e.fieldName} value={e.result} />
                      ))}
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
          ) : (
            ""
          )}
        </Grid>
      </Container>
      <div className={classOcrJava.root}>
        <Tooltip title="Setting OCR">
          <Fab
            className={classOcrJava.fab}
            color="primary"
            aria-label="add"
            size="medium"
            onClick={() => setOpenBackdrop(!backdropOpen)}
          >
            <Settings />
          </Fab>
        </Tooltip>
      </div>
      <Backdrop style={{ zIndex: "1" }} open={backdropOpen}>
        <React.Suspense fallback={null}>
          <Setting
            setOpenBackdrop={setOpenBackdrop}
            backDropOpen={backdropOpen}
            documentType={documentType}
            setType={setType}
          />
        </React.Suspense>
      </Backdrop>
    </>
  );
});

export default OCR_JAVA;
