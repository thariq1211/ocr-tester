import React, { useState } from "react";
import Axios from "axios";
import {
  Container,
  FormControl,
  Card,
  CardContent,
  FormHelperText,
  Input,
  InputLabel,
  Grid,
  Button,
  Typography,
} from "@material-ui/core";

import useStyles from "../../styles";
import Constanta from "../../constanta";

const OCR_NODE = () => {
  const classes = useStyles();
  const url = Constanta.url_node;
  const [result, setResult] = useState("Result will show here");
  const [data, setData] = useState("");
  // const [urlKtp, setUrlKtp] = useState();

  const prosesOcr = async () => {
    setData("");
    const input = document.getElementById("fileInput").files;
    if (input.length === 1) {
      try {
        setResult("Uploading ktp..");
        const formData = new FormData();
        for (var i = 0; i !== input.length; i++) {
          formData.append("file", input[i]);
        }
        const upload = await Axios({
          url: "https://iswitch.ddns.net:5443/upload/ktp",
          method: "POST",
          data: formData,
        });
        if (upload.status === 200 && upload.data.file) {
          setResult("Scanning KTP");
          const response = await Axios({
            url: `${url}/api/ocr/azure/scan_ktp`,
            method: "POST",
            data: { ktp_url: upload.data.file },
          });
          if (response.status === 200) {
            setResult("Successful scan..");
            const { teks } = response.data;
            setTimeout(() => setResult(teks), 3000);
          }
        } else {
          setResult("Something went wrong, please try again");
        }
      } catch (error) {
        setResult(error.message);
      }
    } else {
      setResult("Please input required file");
    }
  };

  return (
    <Container fixed>
      <br />
      <Typography variant="h5">OCR Nodejs</Typography>
      <br />
      <Grid container spacing={2} justify="flex-start" alignItems="center">
        <Grid item>
          <FormControl style={{ width: "40vw" }}>
            <InputLabel htmlFor="fileInput">File OCR</InputLabel>
            <Input
              id="fileInput"
              type="file"
              aria-describedby="my-helper-text"
              onChange={(e) => {
                const output = document.getElementById("output");
                if (e.target.files.length > 0) {
                  output.src = URL.createObjectURL(e.target.files[0]);
                } else {
                  output.src = "";
                }
                output.onload = function () {
                  URL.revokeObjectURL(output.src); // free memory
                };
              }}
            />
            <FormHelperText id="my-helper-text">
              Please input required file
            </FormHelperText>
          </FormControl>
          {/* <FormControl style={{ width: "40vw" }}>
            <InputLabel htmlFor="fileInput">URL KTP</InputLabel>
            <Input
              id="fileInput"
              type="text"
              aria-describedby="my-helper-text"
              onChange={(e) => setUrlKtp(e.target.value)}
            />
            <FormHelperText id="my-helper-text">
              Please input url ktp
            </FormHelperText>
          </FormControl> */}
        </Grid>
        <Grid item>
          <Button color="primary" variant="contained" onClick={prosesOcr}>
            Scan
          </Button>
        </Grid>
        <Grid item sm={12} xs={12} lg={6} md={6}>
          <img id="output" alt="" style={{ width: "100%" }} />
        </Grid>
      </Grid>
      <br />
      <br />
      <Card className={classes.card} variant="outlined">
        <CardContent>
          <Typography
            className={classes.title}
            color="textSecondary"
            gutterBottom
          >
            Result
          </Typography>
          <Typography variant="subtitle1">{result}</Typography>
          <Typography variant="subtitle1">{data}</Typography>
        </CardContent>
      </Card>
    </Container>
  );
};

export default OCR_NODE;
