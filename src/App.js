import React, { useState, memo } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  useHistory,
} from "react-router-dom";
import "./App.css";
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Menu,
  MenuItem,
  Button,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import useStyles from "./styles";

import OcrProvider from "./Pages/OCR.JAVA/context";

const OCR_NET = React.lazy(() => import("../src/Pages/OCR.NET"));
const OCR_JAVA = React.lazy(() => import("../src/Pages/OCR.JAVA"));
const OCR_NODE = React.lazy(() => import("../src/Pages/OCR.NODEJS"));
const FR_NET = React.lazy(() => import("../src/Pages/FR.NET"));

function AppBarComponent() {
  const classes = useStyles();
  const history = useHistory();
  const [anchorEl, setAnchorEl] = useState(null);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon onClick={handleClick} />
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem
              onClick={() => {
                history.push("/ocr_net");
                handleClose();
              }}
            >
              OCR .NET
            </MenuItem>
            <MenuItem
              onClick={() => {
                history.push("/fr_net");
                handleClose();
              }}
            >
              FR .NET
            </MenuItem>
            <MenuItem
              onClick={() => {
                history.push("/ocr_java");
                handleClose();
              }}
            >
              OCR JAVA
            </MenuItem>
            {/* <MenuItem
              onClick={() => {
                history.push("/ocr_node");
                handleClose();
              }}
            >
              OCR Nodejs
            </MenuItem> */}
          </Menu>
        </IconButton>
        <Typography variant="h5" style={{ flexGrow: 1 }}>
          Computer Vision testing
        </Typography>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => history.push("/ocr_node")}
        >
          get em um
        </Button>
      </Toolbar>
    </AppBar>
  );
}

const App = memo(() => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Router>
        <AppBarComponent />
        <Switch>
          <Route path={["/ocr_net", "/"]} exact>
            <React.Suspense fallback={null}>
              <OCR_NET />
            </React.Suspense>
          </Route>
          <Route path="/fr_net">
            <React.Suspense fallback={null}>
              <FR_NET />
            </React.Suspense>
          </Route>
          <Route path="/ocr_java" exact>
            <React.Suspense fallback={null}>
              <OcrProvider>
                <OCR_JAVA />
              </OcrProvider>
            </React.Suspense>
          </Route>
          <Route path="/ocr_node">
            <React.Suspense fallback={null}>
              <OCR_NODE />
            </React.Suspense>
          </Route>
        </Switch>
      </Router>
    </div>
  );
});

export default App;
